var kanal="";
var vzdevek="";

function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').text(sporocilo);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function parseSmilies(sporocilo){
  return sporocilo.html()
          .replace(";)", '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png" />')
          .replace(":)", '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png" />')
          .replace("(y)",'<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png" />')
          .replace(":*", '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png" />')
          .replace(":(", '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png" />')+"<br>"; //replace unici novo vrstico???
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    klepetApp.posljiSporocilo(kanal, sporocilo);
    $('#sporocila').append(parseSmilies(divElementEnostavniTekst(sporocilo)));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

var socket = io.connect();

$(document).ready(function() {
  $('#kanal').text(" @ ");
  var klepetApp = new Klepet(socket);

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      vzdevek=rezultat.vzdevek;
      $('#kanal').text(vzdevek+" @ "+kanal);
      sporocilo = 'Prijavljen si kot ' + vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    if(rezultat.uspesno){
      kanal=rezultat.kanal;
      $('#kanal').text(vzdevek+" @ "+kanal);
      $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
    }
    else{
      $('#sporocila').append(divElementHtmlTekst(rezultat.sporocilo));
    }
  });

  socket.on('sporocilo', function (sporocilo) {
    var novElement = $('<div style="font-weight: bold"></div>').text(sporocilo.besedilo);
    $('#sporocila').append(parseSmilies(novElement));
  });
  
  socket.on('zasebno', function (sporocilo) {
    var novElement = $('<div style="font-weight: bold"></div>').text(sporocilo.besedilo);
    novElement.html(sporocilo.uvod+parseSmilies(novElement));
    $('#sporocila').append(novElement);
  });
  
  socket.on('sistemskoSporocilo', function (sporocilo) {
    var novElement = $('<div style="font-weight: bold"></div>').text(sporocilo.besedilo);
    $('#sporocila').append(novElement);
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();
    for(var u in uporabniki) {
      if (uporabniki[u] != '') {
        $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[u]));
      }
    }
  });
  socket.on('zasebnoNeuspesno', function(sporocilo) {
      $('#sporocila').append(divElementHtmlTekst('Sporočila '+sporocilo.besedilo+' uporabniku z vzdevkom '+
        sporocilo.vzdevek+' ni bilo mogoče posredovati.'));
  });

  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});