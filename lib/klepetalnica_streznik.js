var socketio = require('socket.io');
var fs  = require('fs');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var uporabljeniVzdevki = [];
var trenutniKanal = {};
var kletvice=fs.readFileSync("public/swearWords.csv","utf-8").split(",");
var socketGledeNaVzdevek ={};
var geslaKanalov = {};

exports.listen = function(streznik) {
  io = socketio.listen(streznik);
  io.set('log level', 1);
  io.sockets.on('connection', function (socket) {
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    pridruzitevKanalu(socket, 'Skedenj');
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajPridruzitevKanalu(socket);
    obdelajZasebnoSporocilo(socket);
    socket.on('kanali', function() {
      socket.emit('kanali', io.sockets.manager.rooms);
    });
    socket.on('uporabniki', function() {
      var res=[];
      for (var id in trenutniKanal){
        if(trenutniKanal[id]==trenutniKanal[socket.id]){
          res.push(vzdevkiGledeNaSocket[id]);
        }
      }
      socket.emit('uporabniki',res);
    });
    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
  });
};

function repeat(pattern, count) {
    if (count < 1) return '';
    var result = '';
    while (count > 1) {
        if (count & 1) result += pattern;
        count >>= 1, pattern += pattern;
    }
    return result + pattern;
}

function filtrirajKletvice(sporocilo){
  sporocilo=" "+sporocilo+" ";
  for (var ki in kletvice){
    sporocilo=sporocilo.replace(new RegExp("\\b"+kletvice[ki]+"\\b","gi"),repeat("*",kletvice[ki].length));
  }
  return sporocilo;
}

function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki) {
  var vzdevek = 'Gost' + stGosta;
  vzdevki[socket.id] = vzdevek;
  socketGledeNaVzdevek[vzdevek]=socket;
  socket.emit('vzdevekSpremembaOdgovor', {
    uspesno: true,
    vzdevek: vzdevek
  });
  uporabljeniVzdevki.push(vzdevek);
  return stGosta + 1;
}

function pridruzitevKanalu(socket, kanal) {
  socket.join(kanal);
  trenutniKanal[socket.id] = kanal;
  socket.emit('pridruzitevOdgovor', {kanal: kanal,uspesno: true});
  socket.broadcast.to(kanal).emit('sistemskoSporocilo', {
    besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
  });

  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  if (uporabnikiNaKanalu.length > 1) {
    var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if (uporabnikSocketId != socket.id) {
        if (i > 0) {
          uporabnikiNaKanaluPovzetek += ', ';
        }
        uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
      }
    }
    uporabnikiNaKanaluPovzetek += '.';
    socket.emit('sistemskoSporocilo', {besedilo: uporabnikiNaKanaluPovzetek});
  }
}

function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Gost') == 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo začeti z "Gost".'
      });
    } else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        uporabljeniVzdevki.push(vzdevek);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        socketGledeNaVzdevek[vzdevek] = socket;
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: vzdevek
        });
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sistemskoSporocilo', {
          besedilo: prejsnjiVzdevek + ' se je preimenoval v ' + vzdevek + '.'
        });
      } else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je že v uporabi.'
        });
      }
    }
  });
}

function obdelajPosredovanjeSporocila(socket) {
  socket.on('sporocilo', function (sporocilo) {
    socket.broadcast.to(sporocilo.kanal).emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ': ' + filtrirajKletvice(sporocilo.besedilo)
    });
  });
}

function obdelajZasebnoSporocilo(socket){
  socket.on('zasebno', function(sporocilo){
    if(uporabljeniVzdevki.indexOf(sporocilo.vzdevek)!=-1 && socketGledeNaVzdevek[sporocilo.vzdevek].id!=socket.id){
      socketGledeNaVzdevek[sporocilo.vzdevek].emit('zasebno', {
        besedilo: filtrirajKletvice(sporocilo.besedilo),
        uvod: vzdevkiGledeNaSocket[socket.id] + ' (zasebno): '
      });
    }
    else{
      socket.emit('zasebnoNeuspesno',sporocilo);
    }
  });
}

function obdelajPridruzitevKanalu(socket) {
  socket.on('pridruzitevZahteva', function(kan) {
    var kanal=kan.novKanal;
    var geslo=kan.novoGeslo;
    var kanalZeObstaja = io.sockets.clients(kanal).length>0;
    if(kanal in geslaKanalov && geslo != geslaKanalov[kanal]){
      socket.emit('pridruzitevOdgovor', {uspesno: false, sporocilo: 'Pridružitev v kanal '+kanal+' ni bila uspešna, ker je geslo napačno!'});
    }
    else if(!(kanal in geslaKanalov) && geslo===false  && kanalZeObstaja ||
        kanal in geslaKanalov && geslo==geslaKanalov[kanal] || !kanalZeObstaja){
      if (io.sockets.clients(trenutniKanal[socket.id]).length==1){
        delete geslaKanalov[trenutniKanal[socket.id]];
      }
      socket.leave(trenutniKanal[socket.id]);
      pridruzitevKanalu(socket, kanal);
      if(geslo!==false){
        geslaKanalov[kanal]=geslo;
      }
      
    }
    else{
      socket.emit('pridruzitevOdgovor', {uspesno: false, sporocilo: 'Izbrani kanal '+kanal+
      ' je prosto dostopen in ne zahteva prijave z geslom,zato se prijavite z uporabo /pridruzitev <kanal> ali zahtevajte kreiranje kanala z drugim imenom.'});
    }
    
  });
}

function obdelajOdjavoUporabnika(socket) {
  socket.on('odjava', function() {
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    delete uporabljeniVzdevki[vzdevekIndeks];
    delete socketGledeNaVzdevek[vzdevkiGledeNaSocket[socket.id]];
    delete vzdevkiGledeNaSocket[socket.id];
  });
}