var Klepet = function(socket) {
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.spremeniKanal = function(kanal,geslo) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal,
    novoGeslo: geslo
  });
};

Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  var ukaz1 = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz1) {
    case 'pridruzitev':
      ukaz=ukaz.substring(13,ukaz.length).split('" "');
      if(ukaz.length==2 && ukaz[0].charAt(0)=='"' && ukaz[1].charAt(ukaz[1].length-1)=='"'){
        var kanal=ukaz[0].substring(1,ukaz[0].length);
        var geslo=ukaz[1].substring(0,ukaz[1].length-1);
        this.spremeniKanal(kanal,geslo);
      }
      else{
        besede.shift();
        kanal = besede.join(' ');
        this.spremeniKanal(kanal,false);
      }
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      besede.shift();
      ukaz=ukaz.substring(9,ukaz.length).split('" "');
      if (ukaz.length==2 && ukaz[0].charAt(0)=='"' && ukaz[1].charAt(ukaz[1].length-1)){
        vzdevek = ukaz[0].substring(1,ukaz[0].length);
        besede.shift();
        var besedilo = ukaz[1].substring(0,ukaz[1].length-1);
        var nov = divElementEnostavniTekst(besedilo);
        nov.html('(zasebno za '+vzdevek+'): '+parseSmilies(nov));
        $('#sporocila').append(nov);
        $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
        this.socket.emit('zasebno',{
          'vzdevek' : vzdevek,
          'besedilo' : besedilo});
      }
      else{
        sporocilo = 'Neznan ukaz.';
      }
      break;
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};